use std::env;
use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::prelude::*;
use rand::seq::SliceRandom;
use rand;


const EIGHTBALL_MESSAGES: [&str;8] = [
    "no dumbass",
    "obviously not",
    "why'd you even think that?",
    "nah",
    "no",
    "delusion is not the solution :skull:",
    "nuhuh",
    "go live in a fantasy world :unicorn:"
];
const HELP_MESSAGE: &str = include_str!("help.md");

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if !msg.content.starts_with("!") {
            return;
        }
        
        let split = msg.content.split_once(" ");
        if let Some(split) = split {
            if split.0 == "!echo" {
                let err = msg.channel_id.say(&ctx.http, split.1).await;
                if let Err(why) = err {
                    eprintln!("Error sending message: {why:?}");
                }
            }
            
            if split.0 == "!recho" {
                for i in 0..5 {
                    let err = msg.channel_id.say(&ctx.http, format!("{} {}", split.1, i + 1)).await;
                    if let Err(why) = err {
                        eprintln!("Error sending message: {why:?}");
                    }
                }
            }

            if split.0 == "!8ball" {
                let mention = msg.author.mention();
                let emsg = EIGHTBALL_MESSAGES.choose(&mut rand::thread_rng());
                if let Some(m) = emsg {
                    if let Err(why) = msg.channel_id.say(&ctx.http, format!("{} {}", mention, m)).await {
                        eprintln!("Error sending message: {why:?}");
                    }
                }
            }

            if split.0 == "!owoify" {
                let owoified = owoify(split.1);

                if let Err(why) = msg.channel_id.say(&ctx.http, owoified).await {
                    eprintln!("Error sending message: {why:?}");
                }
            }
        } else {
            if msg.content.eq("!help") {
                let err = msg.channel_id.say(&ctx.http, HELP_MESSAGE).await;
                if let Err(why) = err {
                    eprintln!("Error sending message: {why:?}");
                }  
            }

            if msg.content.eq("!ping") {
                if let Err(why) = msg.channel_id.say(&ctx.http, "Pong!").await {
                    eprintln!("Error sending message: {why:?}");
                }
            }
        }
    }
}

#[tokio::main]
async fn main() {
    let token = env::var("DTOKEN");
    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;

    match token {
        Err(_) => eprintln!("Expected DTOKEN in environment"),
        Ok(token) => {
            let client = Client::builder(&token, intents).event_handler(Handler).await;

            if let Ok(mut client) = client {
                if let Err(why) = client.start().await {
                    eprintln!("Error starting: {why:?}");
                } 
            }
        }            
    }

}

fn owoify(s: &str) -> String {
    let mut s = s.to_owned();
    s = s.to_lowercase();

    s = s.replace(".", "");
    s = s.replace(",", "");
    s = s.replace(";", "");
    s = s.replace("'", "");
    s = s.replace("`", "");

    s = s.replace("l", "w");
    s = s.replace("r", "w");

    s = s.replace(" s", " s-s");
    s = s.replace("y ", "y~ ");

    return s;
}
