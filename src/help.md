# oobusnuts bot

Commands:

- !help - print this message
- !echo PHRASE - say PHRASE
- !recho PHRASE - say PHRASE 5 times
- !ping - Ping!
- !8ball PHRASE - Let your dreams come true
- !owoify PHRASE - uwu

## Examples:

- !help

- !echo hello
